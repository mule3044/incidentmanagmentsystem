from django.contrib.auth.models import User
from django import forms
from .models import FieldOfficer, FieldOfficerDispatch
from django.conf import settings


class FieldOfficerForm(forms.ModelForm):

    class Meta:
        model = FieldOfficer
        fields = ['firstName', 'lastName', 'phoneNumber', 'policeStation' ,'status']


class FieldOfficerDispatchForm(forms.ModelForm):
    dispatchedDate = forms.DateField(widget=forms.DateInput(format = '%d/%m/%Y'), input_formats=settings.DATE_INPUT_FORMATS)
    class Meta:
        model = FieldOfficerDispatch
        fields = ['fieldOfficer','dispatchedDate', 'policeStallName']

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'email', 'password']


