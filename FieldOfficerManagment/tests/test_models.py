from django.test import TestCase

from FieldOfficerManagment.models import FieldOfficer,FieldOfficerDispatch

class FieldOfficerModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        FieldOfficer.objects.create(firstName='Shulka', lastName='Shamba')

    def test_firstName_label(self):
        fieldOfficer = FieldOfficer.objects.get(id=1)
        field_label = fieldOfficer._meta.get_field('firstName').verbose_name
        self.assertEqual(field_label, 'firstName')

    def test_lastName_label(self):
        fieldOfficer = FieldOfficer.objects.get(id=1)
        field_label = fieldOfficer._meta.get_field('lastName').verbose_name
        self.assertEqual(field_label, 'lastName')

    def test_firstName_max_length(self):
        fieldOfficer = FieldOfficer.objects.get(id=1)
        max_length = fieldOfficer._meta.get_field('firstName').max_length
        self.assertEqual(max_length, 250)

    def test_lastName_max_length(self):
        fieldOfficer = FieldOfficer.objects.get(id=1)
        max_length = fieldOfficer._meta.get_field('lastName').max_length
        self.assertEqual(max_length, 250)



