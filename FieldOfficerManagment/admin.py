# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from FieldOfficerManagment.models import FieldOfficer, FieldOfficerDispatch

admin.site.register(FieldOfficer)
admin.site.register(FieldOfficerDispatch)