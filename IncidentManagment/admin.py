# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from IncidentManagment.models import IncidentType, Incident

admin.site.register(IncidentType)
admin.site.register(Incident)