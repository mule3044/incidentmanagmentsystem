from django.contrib.auth.models import User
from django import forms
from .models import Incident, IncidentType
from django.conf import settings


class IncidentTypeForm(forms.ModelForm):

    class Meta:
        model = IncidentType
        fields = ['name', 'remark']


class IncidentForm(forms.ModelForm):
    incidentDate = forms.DateField(widget=forms.DateInput(format = '%d/%m/%Y'), input_formats=settings.DATE_INPUT_FORMATS)
    class Meta:
        model = Incident
        fields = ['incidentType', 'placeName','woreda',
                 'callerName','phoneNumber','description',
                 'incidentDate','latitude','longitude','status']
                 







